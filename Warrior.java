
public class Warrior extends PNJ {
	
	int stamina;
	
	public Warrior(String name) {
		super(name);
		abilities = new Ability[] {new Ability("Shield bash", 10, 30), new Ability("Cleave", 20, 50), new Ability("Omnislash", 50, 90)};
		health = 120;
		stamina = 100;
		belt = new Item[] {new Sword(), new Potion(), new Potion()};
	}

	public int getStamina() {
		return stamina;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	
	@Override
	public String getStatus() {
		return super.getStatus() + " y " + stamina + " de stamina";
	}
	
	@Override
	public void spendResource(int cost) {
		stamina = stamina - cost;
	}
	
	@Override
	public void attackTo(PNJ target) {
		stamina = stamina - 10;
		super.attackTo(target);
	}
	
	public void attackTo(Wizard target) {
		System.out.println(name + " ataca a " + target.getName());
		target.sufferWounds();
	}
	
	@Override
	public String toString() {
		String abilitiesString = "";
		for (int i = 0; i < abilities.length; i++) {
			Ability ability = abilities[i];
			abilitiesString = abilitiesString + " " + ability.toString();
		}
		return getStatus() + ". Mis habilidades: " + abilitiesString;
	}
}
