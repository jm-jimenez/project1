import java.util.Scanner;

public class Utils {

	public int validateRange(int min, int max) {
		boolean isValidOption = false;
		Scanner scanner = new Scanner(System.in);
		int result = -1;
		while (!isValidOption) {
			int opcion = scanner.nextInt();
			scanner.nextLine();
			if (opcion < min || opcion > max) {
				System.out.println("Introduzca un valor entre " + min + " y " + max + ".");
			} else {
				result = opcion;
				isValidOption = true;
			}
		}
		return result;
	}
}
