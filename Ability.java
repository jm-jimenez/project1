
public class Ability {
	String name;
	int cost;
	int dmg;
	
	Ability(String name, int cost, int dmg) {
		this.name = name;
		this.cost = cost;
		this.dmg = dmg;
	}
	
	@Override
	public String toString() {
		return "Ability " + name + ", cost " + cost + ", dmg " + dmg;
	}
}
