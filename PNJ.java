import java.util.Random;

public class PNJ {

	String name;
	int health;
	Ability[] abilities;
	Item[] belt;
	
	PNJ(String name) {
		this.name = name;
	}
	
	public String getStatus() {
		return name + " tiene " + health + " de vida";
	}
	
	public void sufferWounds() {
		int dmg = 20;
		health = health - dmg;
		System.out.println(name + " sufre " + dmg + " de da�o.");
	}
	
	public void sufferWounds(int dmg) {
		health = health - dmg;
		System.out.println(name + " sufre " + dmg + " de da�o.");
	}
	
	public void attackTo(PNJ target) {
		target.sufferWounds();
	}
	
	public void spendResource(int cost) {
		System.out.println("Oye este m�todo lo tienen que implementar los hijos porque no conozco resource.");
	}
	
	public void drinkPotion() {
		health += 50;
		boolean updatedPerformed = false;
		for (int i = 0; i < belt.length && updatedPerformed == false; i++) {
			Item current = belt[i];
			if (current instanceof Potion) {
				Potion potion = (Potion)current;
				if (!potion.isUsed) {
					potion.isUsed = true;
					updatedPerformed = true;
				}
			}
		}
	}
	
	public void attackTo(PNJ target, int dmg) {
		int random = new Random().nextInt(20) + 1;
		if (random < 3) {
			System.out.println("No es muy efectivo...");
			dmg = dmg / 2;
		} else if (random > 19) {
			System.out.println("ES S�PER EFECTIVO!");
			dmg = dmg * 2;
		}
		target.sufferWounds(dmg);
	}
	
	public void drinkPoti() {
		this.health = health + 50;
	}
	
	public void castAbility(Ability ability, PNJ target) {
		System.out.println(name + " se prepara para lanzar " + ability.name);
		spendResource(ability.cost);
		attackTo(target, ability.dmg);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
}
