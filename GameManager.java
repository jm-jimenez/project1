import java.util.Scanner;

public class GameManager {

	Warrior player;
	Scanner scanner = new Scanner(System.in);
	Wizard IA1 = new Wizard("Gandalf");
	boolean isFirstTime = true;
	boolean isPlayerTurn = true;
	
	
	public void startGame() {
		
		System.out.println("Introduzca nombre del personaje");
		player = new Warrior(scanner.nextLine());
		
		System.out.println("Bienvenido " + player.name);
		showMainMenu();
	}
	
	
	public void showMainMenu() {
		if (isPlayerTurn) {
			if (isFirstTime) {
				System.out.println("Qu� quieres hacer?\n0.- Salir\n1.- Atacar\n2.- Beber poci�n");
				isFirstTime = false;
			} else {
				System.out.println("Y ahora... qu� quieres hacer?\n0.- Salir\n1.- Atacar\n2.- Beber poci�n");
			}

			int opcion = new Utils().validateRange(0, 2);
			if (opcion == 1) {
				showAttackMenu();
			} else if (opcion == 2) {
				showPotionsMenu();
			}
		} else {
			iaPlaysItsTurn();
		}
	}
	
	
	public void showAttackMenu() {
		System.out.println("Qu� habilidad quieres lanzar?\n0.- Atr�s");
		for (int i = 0; i < player.abilities.length; i++) {
			System.out.println((i+1) + ".- " + player.abilities[i].name);
		}
		int opcion = new Utils().validateRange(0, player.abilities.length);
		if (opcion == 0) {
			showMainMenu();
		} else if (opcion == 1 || opcion == 2 || opcion == 3) {
			player.castAbility(player.abilities[opcion - 1], IA1);
			endTurn();
			showMainMenu();
		}
	}

	public void showPotionsMenu() {
		int availablePotions = 0;
		for (int index = 0; index < player.belt.length; index++) {
			Item current = player.belt[index];
			if (current instanceof Potion) {
				Potion potion = (Potion)current;
				if (!potion.isUsed) {
					availablePotions ++;
				}
			}
		}
		System.out.println("Tienes " + availablePotions + " pociones disponibles.");
		
		if (availablePotions > 0) {
			System.out.println("�Quieres beber una poci�n?\n1.- S�\n2.- No");
			int opcion = new Utils().validateRange(1, 2);
			if (opcion == 1) {
				player.drinkPotion();
				endTurn();
				showMainMenu();
			} else if (opcion == 2) {
				showMainMenu();
			}
		} else {
			System.out.println("No quedan pociones disponibles!");
			showMainMenu();
		}
	}
	
	public void iaPlaysItsTurn() {
		IA1.attackTo(player);
		endTurn();
		showMainMenu();
	}
	
	public void endTurn() {
		isPlayerTurn = !isPlayerTurn;
	}
	
}
