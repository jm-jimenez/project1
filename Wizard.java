
public class Wizard extends PNJ {
	
	int mana;
	
	Wizard(String name) {
		super(name);
		abilities = new Ability[] {new Ability("Magic missile", 10, 30), new Ability("Arcane comet", 20, 50), new Ability("Pyroblast", 50, 90)};
		health = 80;
		mana = 120;
	}
	
	@Override
	public String getStatus() {
		return super.getStatus() + " y " + mana + " de mana";
	}
	
	
	@Override
	public void attackTo(PNJ target) {
		mana = mana - 20;
		super.attackTo(target);
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}
	
	@Override
	public String toString() {
		
		String abilitiesString = "";
		for (int i = 0; i < abilities.length; i++) {
			abilitiesString = abilitiesString + " " + abilities[i].toString();
		}
		
		return  getStatus() + ". Mis habilidades: " + abilitiesString;
	}
}
