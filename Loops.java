
public class Loops {

	void example() {
		int[][] matriz = new int[8][8];
		int contador = 1;
		String[] labelsEjeX = new String[] {"A", "B", "C", "D", "E", "F", "G", "H"};
		for(int fila = 0; fila < matriz.length; fila++) {
			for(int columna = 0; columna < matriz[fila].length; columna++) {
				matriz[fila][columna] = contador;
				contador++;
			}
		}

		String texto = "    ";
		for (int i = 0; i < labelsEjeX.length; i++) {
			texto = texto + labelsEjeX[i] + "  ";
		}
		texto = texto + "\n";

		for(int fila = 0; fila < matriz.length; fila++) {
			texto = texto + (fila + 1) + " |";
			for(int columna = 0; columna < matriz[fila].length; columna++) {
				if (matriz[fila][columna] < 10) {
					texto = texto + " ";
				}
				texto = texto + matriz[fila][columna];
				if (columna != matriz[fila].length - 1) {
					texto = texto + " ";
				}
			}
			texto = texto + "\n";
		}
		System.out.println(texto);
	}
}
